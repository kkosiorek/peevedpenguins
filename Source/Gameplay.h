//
//  Gameplay.h
//  PeevedPenguins
//
//  Created by Konrad Kosiorek on 07.05.2014.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "CCNode.h"

@interface Gameplay : CCNode <CCPhysicsCollisionDelegate>

@end
