//
//  Seal.m
//  PeevedPenguins
//
//  Created by Konrad Kosiorek on 07.05.2014.
//  Copyright (c) 2014 Apportable. All rights reserved.
//

#import "Seal.h"

@implementation Seal
-(void) didLoadFromCCB{
    self.physicsBody.collisionType = @"seal";
}


@end
